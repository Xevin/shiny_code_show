const {div, span} = React.DOM;

var CurrencyItem = React.createClass({
    render: function() {
        var self = this;
        var diff_mod = "";
        if (self.props.data.diff > 0) {
            diff_mod += "item__diff--up";
        } else {
            diff_mod += "item__diff--down";
        }

        return div(
            {className: "currency__item"},
            div({className: "item__left-wrap"},
                span({className: "item__title"},
                    self.props.data.code
                ),
                span(
                    {className: "item__date"},
                    self.props.data.date
                )
            ),
            div({className: "item__right-wrap"},
                span(
                    {className: "item__tax"},
                    self.props.data.tax
                ),
                span({className: "item__diff"},
                    span({className: diff_mod}),
                    self.props.data.diff
                )
            )
        )
    }
});

var CurrencyList = React.createClass({
    getInitialState: function(){
        return { items: [], wait: true };
    },
    componentDidMount: function() {
        var self = this;

        $.getJSON("/api/get_currency/", function(result){
            if(!result || !result.data || !result.data.length){
                return;
            }

            self.setState({ items: result.data, wait: false });
        });
    },
    render: function() {
        var self = this;

        var currency_items = self.state.items.map(function(e, index) {
            return React.createElement(
                CurrencyItem,
                {key:index, data:e}
            )
        });

        if(!currency_items.length) {
            currency_items = div({className: "currency--wait"});
        }

        return div(
            {className: "currency"},
            currency_items
        )
    }
});

ReactDOM.render(
    React.createElement(
        CurrencyList
    ),
    document.getElementById('currency__list')
);
