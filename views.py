# coding: utf-8

from __future__ import unicode_literals

from django.conf import settings
from django.db.models import Q
from django.forms.formsets import formset_factory
from django.shortcuts import render
from django.views.generic import ListView, DetailView

from tourprom.custom_utils.func import send_html_email

from .forms import AddAgencyForm, AgencyAddressForm
from .models import Agency, AgencyAddress, AgencyAddRequest


class AgencySearchView(ListView):
    model = Agency
    paginate_by = 10

    def get_queryset(self, *args, **kwargs):

        q = self.request.GET.get('q')
        if q and len(q) >= 3:
            result = self.model.objects.filter(
                approved=True
            ).filter(
                Q(inn__icontains=q) |
                Q(name__icontains=q) |
                Q(registry_number__icontains=q)
            )

            address_ids = AgencyAddress.objects.filter(
                address__icontains=q,
                approved=True
            ).values_list("agency", flat=True)

            result2 = Agency.objects.filter(
                approved=True,
                id__in=address_ids
            )

            return result | result2
        else:
            return self.model.objects.none()

    def get_context_data(self, *args, **kwargs):
        context = super(AgencySearchView, self)\
            .get_context_data(*args, **kwargs)

        context['search_query'] = self.request.GET.get('q') or ''
        return context


class AgencyDetailView(DetailView):
    model = Agency
    slug_field = 'inn'


def agency_add_view(request):

    AgencyAddressFormSet = formset_factory(
        AgencyAddressForm,
        extra=1,
        max_num=5
    )

    if request.method == 'POST':
        form = AddAgencyForm(request.POST)

        formset = AgencyAddressFormSet(request.POST)
        if formset.is_valid() and form.is_valid():
            cdata_set = formset.cleaned_data
            cdata = form.cleaned_data

            agency = Agency(
                name=cdata['name'],
                inn=cdata['inn'],
                site=cdata['site'],
                approved=False
            )
            agency.save()

            for addr in cdata_set:
                if 'address' in addr.keys():
                    agency_address = AgencyAddress(
                        agency=agency,
                        address=addr["address"]
                    )
                    agency_address.save()

            add_request = AgencyAddRequest(
                email=cdata['email'],
                description=cdata['description'],
                agency=agency
            )
            add_request.save()

            send_html_email(
                'agency_registry/email/pre_moderate_for_user.html',
                [cdata["email"], ],
                {
                    "subject": "Заявка на добавление турагентства «%s»" % agency.name,
                    "agency": agency
                }
            )

            send_html_email(
                'agency_registry/email/pre_moderate_for_ator.html',
                settings.ATOR_EMAILS,
                {
                    "subject": "Заявка на добавление в Каталог Турагентств России",
                    "req": add_request
                }
            )

            return render(
                request,
                "agency_registry/add_agency.html",
                {
                    "agency": agency,
                    "add_request": add_request,
                    "success": True
                }
            )
    else:
        form = AddAgencyForm()
        formset = AgencyAddressFormSet()

    return render(
        request,
        'agency_registry/add_agency.html',
        {
            'form': form,
            'formset': formset,
            "success": False
        }
    )
