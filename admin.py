# coding: utf-8

from __future__ import unicode_literals

from django import forms
from django.contrib import admin
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
# from django.contrib.auth.decorators import permission_required
from django.utils.safestring import mark_safe

from tourprom.custom_utils.func import get_admin_url

from .models import Agency, AgencyAddress, AgencyAddRequest


@staff_member_required
def decline_agency_add_request(request, pk):
    from tourprom.custom_utils.func import send_html_email

    req = get_object_or_404(AgencyAddRequest, id=pk)
    req.declined = True
    req.approved = False
    req.save()

    send_html_email(
        "agency_registry/email/declined_for_user.html",
        [req.email, ],
        {
            "subject": "Ваша заявка в Каталог Турагентств отклонена",
            "req": req
        }
    )

    return HttpResponseRedirect(get_admin_url(req, req.id))


@staff_member_required
def approve_agency_add_request(request, pk):
    from tourprom.custom_utils.func import send_html_email

    req = get_object_or_404(AgencyAddRequest, id=pk)
    req.declined = False
    req.approved = True
    req.agency.approved = True
    req.agency.save()
    req.save()

    send_html_email(
        "agency_registry/email/approved_for_user.html",
        [req.email, ],
        {
            "subject": "Вы включены в Каталог Турагентств России",
            "req": req
        }
    )

    return HttpResponseRedirect(get_admin_url(req, req.id))


class AgencyAddressAdminForm(forms.ModelForm):

    class Meta:
        model = AgencyAddress
        widgets = {
            'address': forms.TextInput(attrs={'size': '120'})
        }


class AgencyAddressAdmin(admin.StackedInline):
    model = AgencyAddress
    extra = 1
    form = AgencyAddressAdminForm


class AgencyAdmin(admin.ModelAdmin):
    inlines = [AgencyAddressAdmin, ]
    list_display = [
        'id',
        'inn',
        'name',
        'registry_number',
        'approved',
    ]


class AgencyAddRequestAdmin(admin.ModelAdmin):
    list_filter = [
        "approved",
    ]
    list_display = [
        "id",
        "created",
        "email",
        "agency",
        "status"
    ]
    readonly_fields = (
        'email',
        'created',
        'modified',
        'link_to_agency',
        'status',
        'link_to_decline',
    )
    fields = [
        'status',
        'email',
        'description',
        'created',
        'modified',
        'link_to_agency',
        # 'approved',
        'link_to_decline',
    ]

    def status(self, obj):
        stat_tpl = """<span style="color: %s">%s</span>"""

        if obj.approved:
            stat = stat_tpl % ("green", "Прошла модерацию")
        elif obj.declined:
            stat = stat_tpl % ("red", "Отклонена")
        else:
            stat = stat_tpl % ("gray", "Ожидает решения")

        return mark_safe(stat)
    status.short_description = "Статус"
    status.allow_tags = True

    def link_to_agency(self, obj):
        link = """«<a href="%s">%s</a>»""" % (
            get_admin_url(obj.agency, obj.agency.id), obj.agency.name)
        return mark_safe(link)
    link_to_agency.short_description = "Добавленное агентство"

    def link_to_decline(self, obj):
        decline_link_html = """<a href="%sdecline/">Отклонить</a>""" % get_admin_url(
            obj, obj.id)
        approve_link_html = """<a href="%sapprove/">Одобрить</a>""" % get_admin_url(
            obj, obj.id)

        link_html = decline_link_html + \
            """<span style="padding: 0 2em"></span>""" + approve_link_html
        return mark_safe(link_html)
    link_to_decline.short_description = "Действия"

admin.site.register(Agency, AgencyAdmin)
admin.site.register(AgencyAddRequest, AgencyAddRequestAdmin)
