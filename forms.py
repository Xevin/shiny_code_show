# coding: utf-8

from __future__ import unicode_literals

from django import forms

from .models import Agency, AgencyAddress


class AddAgencyForm(forms.Form):
    name = forms.CharField(label="Название фирмы")
    inn = forms.CharField(label="ИНН")
    site = forms.URLField(label="Сайт фирмы", required=False)
    email = forms.EmailField(
        label="Ваш емейл для связи", required=True)
    description = forms.CharField(
        label="основные туроператоры, " +
        "с которым турагентство имеет договорные отношения",
        widget=forms.Textarea(),
        required=False
    )

    def clean_inn(self):
        data = self.cleaned_data['inn']
        same_agencies = Agency.objects.filter(inn=data, approved=True)
        if same_agencies.count():
            raise forms.ValidationError("Уже есть фирма с указанным ИНН")
        else:
            return data

    class Meta:
        pass


class AgencyAddressForm(forms.ModelForm):

    class Meta:
        model = AgencyAddress
        fields = ("address", )
        widgets = {
            "address": forms.TextInput(attrs={"size": "50"})
        }
